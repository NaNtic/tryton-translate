# 
msgid ""
msgstr "Content-Type: text/plain; charset=utf-8\n"

msgctxt "error:purchase.request.create_purchase:"
msgid "Purchase price is missing for product: %s (id: %s)!"
msgstr "Falta el precio de compra del producto: %s (id: %s)!"

msgctxt "error:purchase.request.create_purchase:"
msgid "This price is necessary for creating purchase."
msgstr "Este precio se necesita para creación de compras."

msgctxt "error:purchase.request:"
msgid "Purchase requests are only created by the system."
msgstr "Los requisitos de compras son creados por el sistema únicamente."

msgctxt "error:purchase.request:"
msgid "The requested quantity must be greater than 0"
msgstr "La cantidad solicitada debe ser mayor que 0"

msgctxt "error:stock.order_point:"
msgid "Maximal quantity must be bigger than Minimal quantity"
msgstr "La cantidad máxima debe ser mayor que la cantidad mínima"

msgctxt "error:stock.order_point:"
msgid "Only one order point is allowed for each product-location pair."
msgstr ""
"Solamente se permite un punto de orden para cada par producto-locación."

msgctxt "error:stock.order_point:"
msgid ""
"You can not define two order points on the same product with opposite "
"locations."
msgstr ""
"No puede definir dos puntos de orden del mismo producto con lugares "
"opuestos."

msgctxt "field:product.product,order_points:"
msgid "Order Points"
msgstr "Puntos de Orden"

msgctxt "field:purchase.request,company:"
msgid "Company"
msgstr "Compañía"

msgctxt "field:purchase.request,computed_quantity:"
msgid "Computed Quantity"
msgstr "Cantidad Calculada"

msgctxt "field:purchase.request,computed_uom:"
msgid "Computed UOM"
msgstr "UOM Calculada"

msgctxt "field:purchase.request,create_date:"
msgid "Create Date"
msgstr "Fecha de Creación"

msgctxt "field:purchase.request,create_uid:"
msgid "Create User"
msgstr "Creado por Usuario"

msgctxt "field:purchase.request,id:"
msgid "ID"
msgstr "ID"

msgctxt "field:purchase.request,origin:"
msgid "Origin"
msgstr "Origen"

msgctxt "field:purchase.request,party:"
msgid "Party"
msgstr "Tercero"

msgctxt "field:purchase.request,product:"
msgid "Product"
msgstr "Producto"

msgctxt "field:purchase.request,purchase:"
msgid "Purchase"
msgstr "Compra"

msgctxt "field:purchase.request,purchase_date:"
msgid "Best Purchase Date"
msgstr "Mejor Fecha de Compra"

msgctxt "field:purchase.request,purchase_line:"
msgid "Purchase Line"
msgstr "Línea de Compra"

msgctxt "field:purchase.request,quantity:"
msgid "Quantity"
msgstr "Cantidad"

msgctxt "field:purchase.request,rec_name:"
msgid "Name"
msgstr "Nombre"

msgctxt "field:purchase.request,state:"
msgid "State"
msgstr "Estado"

msgctxt "field:purchase.request,stock_level:"
msgid "Stock at Supply Date"
msgstr "Almacenar al Momento en la fecha de abastecimiento"

msgctxt "field:purchase.request,supply_date:"
msgid "Expected Supply Date"
msgstr "Fecha Esperada de Abastecimiento"

msgctxt "field:purchase.request,uom:"
msgid "UOM"
msgstr "UDM"

msgctxt "field:purchase.request,warehouse:"
msgid "Warehouse"
msgstr "Depósito"

msgctxt "field:purchase.request,warehouse_required:"
msgid "Warehouse Required"
msgstr "Depósito Requerido"

msgctxt "field:purchase.request,write_date:"
msgid "Write Date"
msgstr "Fecha de Modificación"

msgctxt "field:purchase.request,write_uid:"
msgid "Write User"
msgstr "Modificado por Usuario"

msgctxt "field:purchase.request.create.start,id:"
msgid "ID"
msgstr "ID"

msgctxt "field:purchase.request.create_purchase.ask_party,company:"
msgid "Company"
msgstr "Compañía"

msgctxt "field:purchase.request.create_purchase.ask_party,id:"
msgid "ID"
msgstr "ID"

msgctxt "field:purchase.request.create_purchase.ask_party,party:"
msgid "Supplier"
msgstr "Proveedor"

msgctxt "field:purchase.request.create_purchase.ask_party,product:"
msgid "Product"
msgstr "Producto"

msgctxt "field:purchase.request.create_purchase.ask_term,company:"
msgid "Company"
msgstr "Compañía"

msgctxt "field:purchase.request.create_purchase.ask_term,id:"
msgid "ID"
msgstr "ID"

msgctxt "field:purchase.request.create_purchase.ask_term,party:"
msgid "Supplier"
msgstr "Proveedor"

msgctxt "field:purchase.request.create_purchase.ask_term,payment_term:"
msgid "Payment Term"
msgstr "Forma de Pago"

msgctxt "field:stock.order_point,company:"
msgid "Company"
msgstr "Compañía"

msgctxt "field:stock.order_point,create_date:"
msgid "Create Date"
msgstr "Fecha de Creación"

msgctxt "field:stock.order_point,create_uid:"
msgid "Create User"
msgstr "Creado por Usuario"

msgctxt "field:stock.order_point,id:"
msgid "ID"
msgstr "ID"

msgctxt "field:stock.order_point,location:"
msgid "Location"
msgstr "Locación"

msgctxt "field:stock.order_point,max_quantity:"
msgid "Maximal Quantity"
msgstr "Cantidad Máxima"

msgctxt "field:stock.order_point,min_quantity:"
msgid "Minimal Quantity"
msgstr "Cantidad Mínima"

msgctxt "field:stock.order_point,product:"
msgid "Product"
msgstr "Producto"

msgctxt "field:stock.order_point,provisioning_location:"
msgid "Provisioning Location"
msgstr "Locación de Aprovisionamiento"

msgctxt "field:stock.order_point,rec_name:"
msgid "Name"
msgstr "Nombre"

msgctxt "field:stock.order_point,storage_location:"
msgid "Storage Location"
msgstr "Locación de Almacenamiento"

msgctxt "field:stock.order_point,type:"
msgid "Type"
msgstr "Tipo"

msgctxt "field:stock.order_point,unit:"
msgid "Unit"
msgstr "Unidad"

msgctxt "field:stock.order_point,unit_digits:"
msgid "Unit Digits"
msgstr "Decimales de Unidad"

msgctxt "field:stock.order_point,warehouse_location:"
msgid "Warehouse Location"
msgstr "Locación del Depósito"

msgctxt "field:stock.order_point,write_date:"
msgid "Write Date"
msgstr "Fecha de Modificación"

msgctxt "field:stock.order_point,write_uid:"
msgid "Write User"
msgstr "Modificado por Usuario"

msgctxt "model:ir.action,name:act_order_point_form"
msgid "Order Points"
msgstr "Puntos de Orden"

msgctxt "model:ir.action,name:act_purchase_form"
msgid "Purchases"
msgstr "Compras"

msgctxt "model:ir.action,name:act_purchase_request_create"
msgid "Create Purchase Requests"
msgstr "Crear Ordenes de Compras"

msgctxt "model:ir.action,name:act_purchase_request_form"
msgid "Purchase Requests"
msgstr "Solicitudes de Compra"

msgctxt "model:ir.action,name:act_purchase_request_form_draft"
msgid "Draft Purchase Requests"
msgstr "Orden de Compras en Borrador"

msgctxt "model:ir.action,name:wizard_create_purchase"
msgid "Create Purchase"
msgstr "Crear Compra"

msgctxt "model:ir.cron,name:cron_generate_request"
msgid "Generate Purchase Requests"
msgstr "Generar Orden de Compra"

msgctxt "model:ir.cron,name:cron_shipment_iternal"
msgid "Generate Internal Shipments"
msgstr "Generar Envío Interno"

msgctxt "model:ir.ui.menu,name:menu_order_point_form"
msgid "Order Points"
msgstr "Puntos de Orden"

msgctxt "model:ir.ui.menu,name:menu_purchase_request_create"
msgid "Create Purchase Requests"
msgstr "Crear Ordenes de Compra"

msgctxt "model:ir.ui.menu,name:menu_purchase_request_form"
msgid "Purchase Requests"
msgstr "Ordenes de Compra"

msgctxt "model:ir.ui.menu,name:menu_purchase_request_form_draft"
msgid "Draft Purchase Requests"
msgstr "Orden de Compra en Borrador"

msgctxt "model:purchase.request,name:"
msgid "Purchase Request"
msgstr "Orden de Compra"

msgctxt "model:purchase.request.create.start,name:"
msgid "Create Purchase Request"
msgstr "Crear Orden de Compra"

msgctxt "model:purchase.request.create_purchase.ask_party,name:"
msgid "Create Purchase Ask Party"
msgstr "Crear Pregunta a Tercero de Compra"

msgctxt "model:purchase.request.create_purchase.ask_term,name:"
msgid "Create Purchase Ask Term"
msgstr "Crear Pregunta a Tercero de Término"

msgctxt "model:res.group,name:group_purchase_request"
msgid "Purchase Request"
msgstr "Orden de Compra"

msgctxt "model:res.user,name:user_generate_request"
msgid "Cron Purchase Request"
msgstr "Cron Orden de Compra"

msgctxt "model:res.user,name:user_generate_shipment_internal"
msgid "Cron Intenal Shipment"
msgstr "Cron Envío Interno"

#, fuzzy
msgctxt "model:stock.order_point,name:"
msgid "Order Point"
msgstr "Punto de Orden"

msgctxt "selection:purchase.request,origin:"
msgid "Order Point"
msgstr "Punto de Orden"

msgctxt "selection:purchase.request,state:"
msgid "Cancel"
msgstr "Cancelar"

msgctxt "selection:purchase.request,state:"
msgid "Done"
msgstr "Hecho"

msgctxt "selection:purchase.request,state:"
msgid "Draft"
msgstr "Borrador"

msgctxt "selection:purchase.request,state:"
msgid "Purchased"
msgstr "Comprado"

msgctxt "selection:stock.order_point,type:"
msgid "Internal"
msgstr "Interna"

msgctxt "selection:stock.order_point,type:"
msgid "Purchase"
msgstr "Compra"

msgctxt "view:purchase.request.create.start:"
msgid "Create Purchase Request"
msgstr "Crear Orden de Compra"

msgctxt "view:purchase.request.create.start:"
msgid "Create Purchase Request?"
msgstr "Crear Orden de Compra?"

msgctxt "view:purchase.request.create_purchase.ask_party:"
msgid "Create Purchase: Missing Supplier"
msgstr "Crear Compra: Sin Proveedor"

msgctxt "view:purchase.request.create_purchase.ask_term:"
msgid "Create Purchase: Missing Payment Term"
msgstr "Crear Compra: Sin Término de Pago"

msgctxt "view:purchase.request:"
msgid "Product Info"
msgstr "Información de Producto"

msgctxt "view:purchase.request:"
msgid "Purchase Request"
msgstr "Solicitud de Compra"

msgctxt "view:purchase.request:"
msgid "Purchase Requests"
msgstr "Ordenes de Compra"

msgctxt "view:purchase.request:"
msgid "Supply Info"
msgstr "Información de Proveedor"

msgctxt "view:stock.order_point:"
msgid "Locations"
msgstr "Locaciones"

msgctxt "view:stock.order_point:"
msgid "Order Point"
msgstr "Punto de Orden"

msgctxt "view:stock.order_point:"
msgid "Order Point Type"
msgstr "Tipo de Punto de Orden"

msgctxt "view:stock.order_point:"
msgid "Order Points"
msgstr "Puntos de Orden"

msgctxt "view:stock.order_point:"
msgid "Product Info"
msgstr "Información de Producto"

msgctxt "wizard_button:purchase.request.create,start,create_:"
msgid "Create"
msgstr "Crear"

msgctxt "wizard_button:purchase.request.create,start,end:"
msgid "Cancel"
msgstr "Cancelar"

msgctxt "wizard_button:purchase.request.create_purchase,ask_party,end:"
msgid "Cancel"
msgstr "Cancelar"

msgctxt "wizard_button:purchase.request.create_purchase,ask_party,start:"
msgid "Continue"
msgstr "Continuar"

msgctxt "wizard_button:purchase.request.create_purchase,ask_term,end:"
msgid "Cancel"
msgstr "Cancelar"

msgctxt "wizard_button:purchase.request.create_purchase,ask_term,start:"
msgid "Continue"
msgstr "Continuar"
